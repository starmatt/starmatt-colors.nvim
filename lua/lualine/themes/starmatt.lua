local c = require('starmatt.colors')

local theme = {}

theme.normal = {
    a = { bg = c.green, fg = c.black },
    b = { bg = c.contrast, fg = c.green },
    c = { bg = c.black, fg = c.green },
    x = { bg = c.black, fg = c.gray },
}

theme.insert = {
    a = { bg = c.blue, fg = c.black },
    b = { bg = c.contrast, fg = c.blue },
    c = { bg = c.black, fg = c.blue },
    x = { bg = c.black, fg = c.gray },
}

theme.visual = {
    a = { bg = c.yellow, fg = c.black },
    b = { bg = c.contrast, fg = c.yellow },
    c = { bg = c.black, fg = c.yellow },
    x = { bg = c.black, fg = c.gray },
}

theme.replace = {
    a = { bg = c.magenta, fg = c.black },
    b = { bg = c.contrast, fg = c.magenta },
    c = { bg = c.black, fg = c.magenta },
    x = { bg = c.black, fg = c.gray },
}

theme.command = {
    a = { bg = c.red, fg = c.black },
    b = { bg = c.contrast, fg = c.red },
    c = { bg = c.black, fg = c.red },
    x = { bg = c.black, fg = c.gray },
}

theme.inactive = {
    a = { bg = c.gray, fg = c.black },
    b = { bg = c.contrast, fg = c.gray },
    c = { bg = c.contrast, fg = c.gray },
    x = { bg = c.contrast, fg = c.gray },
}

return theme;
