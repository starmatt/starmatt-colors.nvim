-- ✨ Very own colorscheme for neovim
-- ----------------------------------------------------------------------------
-- Colorscheme:     starmatt
-- Description:     Personal colorscheme for neovim, made in lua!
-- Author:          Matthieu Borde <dev at starmatt dot net>
-- Website:         https://gitlab.com/starmatt/starmatt-colors.nvim

local name = 'starmatt'

-- Setup environment
vim.cmd('hi clear')

vim.g.colors_name = name

vim.opt.background = 'dark'
vim.opt.termguicolors = true

-- Apply the colorscheme
local theme = require(name .. '.theme')
theme.apply()
