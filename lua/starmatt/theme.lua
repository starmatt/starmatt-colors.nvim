local c = require('starmatt.colors')

local syntax = {
    -- a boolean constant: TRUE, false
    Boolean = { fg = c.number },

    -- any character constant: 'c', '\n'
    Character = { fg = c.string },

    -- normal comments
    Comment = { fg = c.comments },

    -- normal if, then, else, endif, switch, etc.
    Conditional = { fg = c.conditional },

    -- any constant
    Constant = { fg = c.constant },

    -- debugging statements
    Debug = { fg = c.red },

    -- preprocessor #define
    Define = { fg = c.include },

    -- character that needs attention like , or .
    Delimiter = { fg = c.delim },

    -- any erroneous construct
    Error = {
        fg = c.error,
        bg = c.none,
        style = 'undercurl'
    },

    -- try, catch, throw
    Exception = { fg = c.exception },

    -- a floating point constant: 2.3e10 Float = { fg = c.number },

    -- normal function names
    Function = { fg = c.func },

    -- any variable name
    Identifier = { fg = c.variable },

    -- left blank, hidden
    Ignore = { fg = c.disabled },

    -- preprocessor #include
    Include = { fg = c.include },

    -- normal for, do, while, etc.
    Keyword = { fg = c.keyword },

    -- case, default, etc.
    Label = { fg = c.keyword },

    -- same as Define
    Macro = { fg = c.include },

    -- a number constant: 5
    Number = { fg = c.number },

    -- sizeof", "+", "*", etc.
    Operator = { fg = c.operator },

    -- preprocessor #if, #else, #endif, etc.
    PreCondit = { fg = c.include },

    -- generic Preprocessor
    PreProc = { fg = c.include },

    -- normal any other keyword
    Repeat = { fg = c.keyword },

    -- any special symbol
    Special = { fg = c.special },

    -- special character in a constant
    SpecialChar = { fg = c.special },

    -- special things inside a comment
    SpecialComment = { bg = c.contrast },

    -- any statement
    Statement = { fg = c.keyword },

    -- static, register, volatile, etc.
    StorageClass = { fg = c.construct },

    -- any string
    String = { fg = c.string },

    -- struct, union, enum, etc.
    Structure = { fg = c.keyword },

    -- you can use CTRL-] on this
    Tag = { fg = c.red },

    -- anything that needs extra attention; mostly the keywords
    Todo = {
        fg = c.warning,
        bg = c.none
    },

    -- int, long, char, etc.
    Type = { fg = c.keyword },

    -- A typedef
    Typedef = { fg = c.include },

    -- text that stands out, HTML links
    Underlined = {
        fg = c.link,
        bg = c.none,
        style = 'underline'
    },
}

local extra = {
    htmlLink = {
        fg = c.link,
        style = 'underline'
    },

    htmlH1 = { fg = c.fg },

    htmlH2 = { fg = c.fg },

    htmlH3 = { fg = c.fg },

    htmlH4 = { fg = c.fg },

    htmlH5 = { fg = c.fg },

    markdownBold = { fg = c.cyan },

    markdownBoldDelimiter = { fg = c.delim },

    markdownCode = { fg = c.pink },

    markdownItalic = { fg = c.magenta },

    markdownItalicDelimiter = { fg = c.delim },

    markdownH1 = { fg = c.blue },

    markdownH1Delimiter = { fg = c.delim },

    markdownH2 = { fg = c.blue },

    markdownH2Delimiter = { fg = c.delim },

    markdownH3 = { fg = c.blue },

    markdownH3Delimiter = { fg = c.delim },
}

local editor = {
    -- placeholder characters substituted for concealed text (see 'conceallevel')
    Conceal = { fg = c.disabled },

    --  used for the column set with 'colorcolumn'
    ColorColumn = {
        fg = c.none,
        bg = c.contrast
    },

    -- Command mode message in the cmdline
    CommandMode = {
        fg = c.fg,
        bg = c.none,
        style = 'reverse',
    },

    -- the character under the cursor
    Cursor = { fg = c.accent },

    -- Screen-column at the cursor
    CursorColumn = {
        fg = c.none,
        bg = c.black
    },

    -- Screen-line at the cursor
    CursorLine = {
        fg = c.none,
        bg = c.black
    },

    -- like Cursor, but used when in IME mode
    CursorIM = {
        fg = c.cursor,
        bg = c.none,
        style = 'reverse'
    },

    -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.
    CursorLineNr = {
        fg = c.fg,
        bg = c.black
    },

    -- diff mode: Added line
    DiffAdd = {
        fg = c.green,
        bg = c.none,
        style = 'reverse'
    },

    --  diff mode: Changed line
    DiffChange = {
        fg = c.blue,
        bg = c.none,
        style = 'reverse'
    },

    -- diff mode: Deleted line
    DiffDelete = {
        fg = c.red,
        bg = c.none,
        style = 'reverse'
    },

    -- diff mode: Changed text within a changed line
    DiffText = {
        fg = c.fg,
        bg = c.none,
        style = 'reverse'
    },

    -- directory names (and other special names in listings)
    Directory = { fg = c.blue },

    -- ~ lines at the end of a buffer
    EndOfBuffer = { fg = c.disabled },

    -- error messages
    ErrorMsg = {
        bg = c.error,
        fg = c.black
    },

    FloatBorder = {
        fg = c.border,
        bg = c.contrast
    },

    -- 'foldcolumn'
    FoldColumn = { fg = c.blue },

    -- line used for closed folds
    Folded = { fg = c.disabled },

    healthError = { fg = c.error },

    healthSuccess = { fg = c.green },

    healthWarning = { fg = c.warning },

    -- 'incsearch' highlighting
    IncSearch = {
        fg = c.none,
        bg = c.none,
        style = 'reverse'
    },

    -- Insert mode message in the cmdline
    InsertMode = {
        fg = c.blue,
        bg = c.none,
        style = 'reverse'
    },

    -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
    LineNr = { fg = c.disabled },

    -- The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
    MatchParen = {
        fg = c.black,
        bg = c.green,
    },

    -- 'showmode' message
    ModeMsg = { fg = c.accent },

    -- |more-prompt|
    MoreMsg = { fg = c.accent },

    -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
    NonText = { fg = c.disabled },

    -- normal text and background color
    Normal = {
        fg = c.fg,
        bg = c.bg
    },

    -- normal text and background color for floating windows
    NormalFloat = {
        fg = c.fg,
        bg = c.contrast
    },

    -- Normal mode message in the cmdline
    NormalMode = {
        fg = c.accent,
        bg = c.none,
        style = 'reverse'
    },

    -- Popup menu: normal item.
    Pmenu = {
        fg = c.fg,
        bg = c.contrast
    },

    -- Popup menu: scrollbar.
    PmenuSbar = {
        fg = c.fg,
        bg = c.contrast
    },

    -- Popup menu: selected item.
    PmenuSel = {
        fg = c.accent,
        bg = c.active,
    },

    -- Popup menu: Thumb of the scrollbar.
    PmenuThumb = {
        fg = c.fg,
        bg = c.border
    },

    -- Line numbers for quickfix lists
    qfLineNr = {
        fg = c.black,
        bg = c.title,
        style = 'reverse'
    },

    -- |hit-enter| prompt and yes/no questions
    Question = { fg = c.fg },

    -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
    QuickFixLine = {
        fg = c.black,
        bg = c.blue,
        style = 'reverse'
    },

    -- Replace mode message in the cmdline
    ReplaceMode = {
        fg = c.red,
        bg = c.none,
        style = 'reverse'
    },

    -- Last search pattern highlighting.
    Search = {
        fg = c.none,
        bg = c.none,
        style = 'reverse'
    },

    SignColumn = {
        fg = c.fg,
        bg = c.bg
    },

    -- Unprintable characters: text displayed differently from what it really is.  But not 'listchars' whitespace. |hl-Whitespace|
    SpecialKey = { fg = c.special },

    -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
    SpellBad = {
        fg = c.error,
        bg = c.none,
        style = 'undercurl'
    },

    -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
    SpellCap = {
        fg = c.blue,
        bg = c.none,
        style = 'undercurl'
    },

    -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
    SpellLocal = {
        fg = c.cyan,
        bg = c.none,
        style = 'undercurl'
    },

    -- Word that is recognized by the spellchecker as one that is hardly ever used.  |spell| Combined with the highlighting used otherwise.
    SpellRare = {
        fg = c.magenta,
        bg = c.none,
    },

    -- status line of current window
    StatusLine = {
        fg = c.accent,
        bg = c.active
    },

    -- status lines of not-current windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
    StatusLineNC = {
        fg = c.fg,
        bg = c.bg
    },

    -- status line of current terminal window
    StatusLineTerm = {
        fg = c.fg,
        bg = c.active
    },

    -- status lines of not-current terminal windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
    StatusLineTermNC = {
        fg = c.fg,
        bg = c.bg
    },

    TabLine = {
        fg = c.fg,
        bg = c.contrast,
    },

    -- tab pages line, where there are no labels
    TabLineFill = {
        fg = c.fg,
        bg = c.black
    },

    -- tab pages line, active tab page label
    TabLineSel = {
        fg = c.black,
        bg = c.accent
    },

    -- titles for output from ":set all", ":autocmd" etc.
    Title = {
        fg = c.title,
        bg = c.none,
        style = 'bold'
    },

    -- ToolbarButton = { fg = c.fg, bg = c.none, style = 'bold' },

    -- ToolbarLine = { fg = c.fg, bg = c.bg_alt },

    -- the column separating vertically split windows
    VertSplit = {
        fg = c.border,
        bg = c.bg
    },

    -- Visual mode selection
    Visual = {
        fg = c.none,
        bg = c.none,
        style = 'reverse'
    },

    -- Visual mode message in the cmdline
    VisualMode = {
        fg = c.none,
        bg = c.none,
        style = 'reverse'
    },

    -- Visual mode selection when vim is "Not Owning the Selection".
    VisualNOS = {
        fg = c.none,
        bg = c.none,
        style = 'reverse'
    },

    -- warning messages
    WarningMsg = { fg = c.warning },


    Warnings = { fg = c.warning },

    -- current match in 'wildmenu' completion
    WildMenu = {
        fg = c.yellow,
        bg = c.none,
    },

    -- "nbsp", "space", "tab" and "trail" in 'listchars'
    Whitespace = {
        fg = c.disabled
    },
}

local treesitter = {
    commentTSConstant = { fg = c.fg },

    -- For C++/Dart attributes, annotations that can be attached to the code to denote some kind of meta information.
    TSAnnotation = { fg = c.red },

    -- (unstable) TODO: docs
    TSAttribute = { fg = c.yellow },

    -- For booleans.
    TSBoolean = { fg = c.number },

    -- For characters.
    TSCharacter = { fg = c.string },

    -- For comment blocks.
    TSComment = { fg = c.comments },

    -- For keywords related to conditionnals.
    TSConditional = { fg = c.keyword },

    -- For constructor calls and definitions: `= { }` in Lua, and Java constructors.
    TSConstructor = { fg = c.construct },

    -- For constants
    TSConstant = { fg = c.constant },

    -- For constant that are built in the language: `nil` in Lua.
    TSConstBuiltin = { fg = c.accent },

    -- For constants that are defined by macros: `NULL` in C.
    TSConstMacro = { fg = c.constant },

    -- For text to be represented with emphasis.
    TSEmphasis = { fg = c.cyan },

    -- For syntax/parser errors.
    TSError = { style = 'undercurl' },

    -- For exception related keywords.
    TSException = { fg = c.exception },

    -- For fields.
    TSField = { fg = c.param },

    -- For floats.
    TSFloat = { fg = c.number },

    -- For builtin functions: `table.insert` in Lua.
    TSFuncBuiltin = { fg = c.func },

    -- For macro defined fuctions (calls and definitions): each `macro_rules` in Rust.
    TSFuncMacro = { fg = c.func },

    -- For fuction (calls and definitions).
    TSFunction = { fg = c.func },

    -- For includes: `#include` in C, `use` or `extern crate` in Rust, or `require` in Lua.
    TSInclude = { fg = c.include },

    -- For keywords that don't fall in previous categories.
    TSKeyword = { fg = c.keyword },

    -- For keywords used to define a fuction.
    TSKeywordFunction = {
        fg = c.func,
        style = 'italic'
    },

    --
    TSKeywordOperator = { fg = c.keyword },

    -- For return keywords
    TSKeywordReturn = {
        fg = c.keyword,
        style = 'italic'
    },

    -- For labels: `label:` in C and `:label:` in Lua.
    TSLabel = { fg = c.keyword },

    -- Literal text
    TSLiteral = { fg = c.fg },

    -- For method calls and definitions.
    TSMethod = { fg = c.func },

    -- For identifiers referring to modules and namespaces.
    TSNamespace = { fg = c.include },

    --TSNone = {}, -- TODO: docs

    -- For all numbers
    TSNumber = { fg = c.number },

    -- For any operator: `+`, but also `->` and `*` in C.
    TSOperator = { fg = c.operator },

    -- For parameters of a function.
    TSParameter = { fg = c.param },

    -- For references to parameters of a function.
    TSParameterReference = { fg = c.param },

    -- Same as `TSField`,accesing for struct members in C.
    TSProperty = { fg = c.variable },

    -- For delimiters ie: `.`
    TSPunctDelimiter = { fg = c.delim },

    -- For brackets and parens.
    TSPunctBracket = { fg = c.punct },

    -- For special punctutation that does not fall in the catagories before.
    TSPunctSpecial = { fg = c.punct },

    -- For keywords related to loops.
    TSRepeat = { fg = c.keyword },

    -- For strikethrough text.
    TSStrike = {},

    -- For strings.
    TSString = { fg = c.string },

    -- For regexes.
    TSStringRegex = { fg = c.string },

    -- For escape characters within a string.
    TSStringEscape = { fg = c.cyan },

    -- For identifiers referring to symbols or atoms.
    TSSymbol = { fg = c.number },

    -- Tags like html tag names.
    TSTag = { fg = c.construct },

    TSTagAttribute = { fg = c.func },

    -- Tag delimiter like `<` `>` `/`
    TSTagDelimiter = { fg = c.delim },

    -- For strings considered text in a markup language.
    TSText = { fg = c.fg },

    -- TSTextReference = { fg = c.yellow }, -- FIXME

    -- Text that is part of a title.
    TSTitle = {
        fg = c.blue,
        bg = c.none,
    },

    -- For types.
    TSType = { fg = c.keyword },

    -- For builtin types.
    TSTypeBuiltin = { fg = c.keyword },

    -- For text to be represented with an underline.
    TSUnderline = {
        fg = c.fg,
        bg = c.none,
        style = 'underline'
    },

    -- Any URI like a link or email.
    TSURI = { fg = c.cyan },

    -- Any variable name that does not have another highlight.
    TSVariable = { fg = c.variable },

    -- Variable names that are defined by the languages, like `this` or `self`.
    TSVariableBuiltin = { fg = c.construct },
}

local lsp = {
    -- used for "Error" diagnostic virtual text
    LspDiagnosticsDefaultError = {
        fg = c.error,
        style = 'italic'
    },

    -- used for "Error" diagnostic signs in sign column
    LspDiagnosticsSignError = { fg = c.error },

     -- used for "Error" diagnostic messages in the diagnostics float
    LspDiagnosticsFloatingError = { fg = c.error },

    -- Virtual text "Error"
    LspDiagnosticsVirtualTextError = {
        fg = c.error,
        style = 'italic'
    },

    -- used to underline "Error" diagnostics.
    LspDiagnosticsUnderlineError = {
        style = 'undercurl',
        sp = c.error
    },

    -- used for "Warning" diagnostic virtual text
    LspDiagnosticsDefaultWarning = {
        fg = c.warning,
        style = 'italic'
    },

    -- used for "Warning" diagnostic signs in sign column
    LspDiagnosticsSignWarning = { fg = c.warning },

    -- used for "Warning" diagnostic messages in the diagnostics float
    LspDiagnosticsFloatingWarning = { fg = c.warning },

    -- Virtual text "Warning"
    LspDiagnosticsVirtualTextWarning = {
        fg = c.warning,
        style = 'italic'
    },

    -- used to underline "Warning" diagnostics.
    LspDiagnosticsUnderlineWarning = {
        style = 'undercurl',
        sp = c.warning
    },

    -- used for "Information" diagnostic virtual text
    LspDiagnosticsDefaultInformation = {
        fg = c.cyan,
        style = 'italic'
    },

    -- used for "Information" diagnostic signs in sign column
    LspDiagnosticsSignInformation =  { fg = c.cyan },

    -- used for "Information" diagnostic messages in the diagnostics float
    LspDiagnosticsFloatingInformation = { fg = c.cyan },

    -- Virtual text "Information"
    LspDiagnosticsVirtualTextInformation = {
        fg = c.cyan,
        style = 'italic'
    },

    -- used to underline "Information" diagnostics.
    LspDiagnosticsUnderlineInformation = {
        style = 'undercurl',
        sp = c.cyan
    },

    -- used for "Hint" diagnostic virtual text
    LspDiagnosticsDefaultHint = {
        fg = c.blue,
        style = 'italic'
    },

    -- used for "Hint" diagnostic signs in sign column
    LspDiagnosticsSignHint = { fg = c.blue },

    -- used for "Hint" diagnostic messages in the diagnostics float
    LspDiagnosticsFloatingHint = { fg = c.blue },

    -- Virtual text "Hint"
    LspDiagnosticsVirtualTextHint = {
        fg = c.blue,
        style = 'italic'
    },

    -- used to underline "Hint" diagnostics.
    LspDiagnosticsUnderlineHint = {
        style = 'undercurl',
        sp = c.blue
    },

    -- used for highlighting "text" references
    LspReferenceText = {
        fg = c.accent,
        bg = c.constrast
    },

    -- used for highlighting "read" references
    LspReferenceRead = {
        fg = c.accent,
        bg = c.constrast
    },

    -- used for highlighting "write" references
    LspReferenceWrite = {
        fg = c.accent,
        bg = c.constrast
    },
}

local plugins = {
    -- https://github.com/romgrk/barbar.nvim#highlighting
    barbar = {
        BufferCurrent = { fg = c.accent, bg = c.bg },
        BufferCurrentIndex = { fg = c.special, bg = c.bg },
        BufferCurrentMod = { fg = c.fg, bg = c.bg, style = 'italic' },
        BufferCurrentSign = { fg = c.accent, bg = c.bg },
        BufferCurrentTarget = { fg = c.special, bg = c.bg },

        BufferVisible = { fg = c.disabled, bg = c.bg },
        BufferVisibleIndex = { fg = c.disabled, bg = c.bg },
        BufferVisibleMod = { fg = c.fg, bg = c.bg, style = 'italic' },
        BufferVisibleSign = { fg = c.disabled, bg = c.bg },
        BufferVisibleTarget = { fg = c.special, bg = c.bg },

        BufferInactive = { fg = c.disabled, bg = c.contrast },
        BufferInactiveIndex = { fg = c.disabled, bg = c.contrast },
        BufferInactiveMod = { fg = c.fg, bg = c.contrast, style = 'italic' },
        BufferInactiveSign = { fg = c.disabled, bg = c.contrast },
        BufferInactiveTarget = { fg = c.special, bg = c.contrast },

        BufferTabpages = { fg = c.contrast, bg = c.black },
        BufferTabpageFill = { fg = c.disabled, bg = c.black },
    },

    -- https://github.com/folke/which-key.nvim#-colors
    ['which-key'] = {
        WhichKey = { fg = c.blue },
        WhichKeyGroup = { fg = c.cyan },
        WhichKeySeparator = { fg = c.delim },
        WhichKeyDesc = { fg = c.comment },
        WhichKeyFloat = { fg = c.fg, bg = c.contrast },
        WhichKeyValue = { fg = c.accent },
    },
}

local hi = function(group, color)
    local hl = {
        'highlight ' .. group,
        color.style and 'gui=' .. color.style or 'gui=NONE',
        color.fg and 'guifg=' .. color.fg or 'guifg=NONE',
        color.bg and 'guibg=' .. color.bg or 'guibg=NONE',
        color.sp and 'guisp=' .. color.sp or ''
    }

    vim.cmd(table.concat(hl, ' '))

    if color.link then
        vim.cmd(table.concat({
            'highlight! link ',
            group,
            color.link
        }, ' '))
    end
end

local keys = function(table)
    local keyset = {}

    for key, _ in pairs(table) do
        keyset[#keyset + 1] = key
    end

    return keyset
end

local apply = function(opts)
    opts = opts or {}
    opts.extra = opts.extra ~= nil and opts.extra or true
    opts.plugins = opts.plugins ~= nil and opts.plugins or keys(plugins)

    for group, color in pairs(syntax) do hi(group, color) end
    for group, color in pairs(editor) do hi(group, color) end
    for group, color in pairs(treesitter) do hi(group, color) end
    for group, color in pairs(lsp) do hi(group, color) end

    if opts.extra then
        for group, color in pairs(extra) do hi(group, color) end
    end

    -- Plugins!
    for _, plugin in pairs(opts.plugins) do
        for group, color in pairs(plugins[plugin]) do hi(group, color) end
    end
end

return {
    apply = apply
}
