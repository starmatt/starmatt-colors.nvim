local colors = {}

-- Colors
colors.bg = '#1a1d23'       -- hsl(220, 15%, 12%)
colors.fg = '#d3d7de'       -- hsl(220, 15%, 85%)

colors.black = '#111317'    -- hsl(220, 15%, 8%)
colors.gray = '#454e5e'     -- hsl(220, 15%, 32%)
colors.darkgray = '#23272f' -- hsl(220, 15%, 16%)

colors.red = '#f53d99'      -- hsl(330, 90%, 60%)
colors.green = '#1ff98c'    -- hsl(150, 95%, 55%)
colors.yellow = '#ffc34d'   -- hsl(40, 100%, 65%)
colors.blue = '#558bf6'     -- hsl(220, 90%, 65%)
colors.cyan = '#8fb7ef'     -- hsl(215, 75%, 75%)
colors.magenta = '#9580ff'  -- hsl(250, 100%, 75%)
colors.pink = '#c871f4'     -- hsl(280, 85%, 70%)

colors.none = 'NONE'

-- Groups
colors.accent = colors.green
colors.active = colors.black
colors.border = colors.black
colors.comments = colors.gray
colors.contrast = colors.darkgray
colors.disabled = colors.gray
colors.error = colors.red
colors.special = colors.pink
colors.warning = colors.yellow

colors.constant = colors.fg
colors.construct = colors.blue
colors.delim = colors.gray
colors.exception = colors.red
colors.func = colors.cyan
colors.include = colors.red
colors.keyword = colors.magenta
colors.number = colors.yellow
colors.operator = colors.pink
colors.param = colors.fg
colors.punct = colors.pink
colors.string = colors.green
colors.variable = colors.fg

return colors
